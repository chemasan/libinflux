/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include "influx.hpp"
#include <iostream>
#include <variant>

using namespace influx;
using namespace std;

int main()
{
	vector<Record> metrics;
	Record metric;
	metric.measurement = "miprueba";
	metric.tags["host"] = "mipc";
	metric.tags["target"] = "mysite.tld";
	metric.fields["status"] = 200l;
	metric.fields["latency"] = 10.0;
	metric.timestamp = 1596474397917527148;
	metrics.push_back(metric);

	metric.fields["latency"] = 20.0;
	metric.timestamp += 5000000000;
	metrics.push_back(metric);

	
	metric.fields["latency"] = 50.0;
	metric.timestamp += 5000000000;
	metrics.push_back(metric);

	metric.fields["latency"] = 7.5;
	metric.timestamp += 5000000000;
	metrics.push_back(metric);

	metric.fields["status"] = 0l;
	metric.fields["latency"] = 1000.0;
	metric.timestamp += 5000000000;
	metrics.push_back(metric);

	metric.fields["status"] = 500l;
	metric.fields["latency"] = 5.0;
	metric.timestamp += 5000000000;
	metrics.push_back(metric);

	metric.fields["status"] = 200l;
	metric.fields["latency"] = 20.0;
	metric.timestamp += 5000000000;
	metrics.push_back(metric);

	for (auto m : metrics) cout << m.toString() << endl;
	Influx server("localhost:8086","myuser","mypass");
	server.createDb("exampledb");
	server.write("exampledb",metrics);
}
