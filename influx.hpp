/*

  MIT License
  
  Copyright (c) 2020 Jose Manuel Sanchez Madrid
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

#ifndef __INFLUX_HPP
#define __INFLUX_HPP 1

#include <string>
#include <vector>
#include <map>
#include <cstdint>
#include <variant>


namespace influx
{
	using namespace std;

	using Data = variant<int64_t, double, string, bool>;
	
	string toString(const Data& value);

	class Record
	{
		public:
		string measurement;
		map<string,string> tags;
		map<string,Data> fields;
		int64_t timestamp;

		string toString();
	};
	
	class Influx
	{
		public:
		Influx(const string& url, const string& user=""s, const string& password=""s);
		Influx(Influx&& other);
		Influx& operator=(Influx&& other);
		~Influx();
		void createDb(const string& dbname);
		void write(const string& dbname, vector<Record>& records, unsigned long maxSize = 25000000);

		Influx() = delete;
		Influx(const Influx& other) = delete;
		Influx& operator=(const Influx& other) = delete;
	
		private:
		string url;
		class Session;
		Session* session = nullptr;
	};
}
#endif
