LibInflux
============
Copyright (c) 2020 Jose Manuel Sanchez Madrid. This file is licensed under MIT license. See file LICENSE for details.

## Overview
LibInflux is a C++ client library for [InfluxDB timesries database 1.8](https://www.influxdata.com/time-series-platform/). To know more about InfluxDB, please read its [documentation](https://docs.influxdata.com/influxdb/v1.8/introduction/get-started/).

## Requirements
In addition to GNU Make and a C++17 capable compiler, the following libraries must be installed first in order to compile LibInflux:
- [CPR](https://github.com/whoshuu/cpr/). CPR itself requires the following libraries:
    - [libcurl](https://github.com/curl/curl/)
    - [OpenSSL](https://github.com/openssl/openssl)
    - [zlib](https://github.com/madler/zlib)
- [JSON for Modern C++](https://github.com/nlohmann/json/)
- [Catch2](https://github.com/catchorg/Catch2/). This is required to build and run the unit tests only.

## Install
Install the required libraries indicated in the previus section.
Then run:
```
make
sudo make install
sudo ldconfig
```

## Usage
To use the library, include the "influx.hpp" header file in your code. All LibInflux classes and functions belong to the "influx" namespace:
```
#include <influx.hpp>

using namespace influx;
```
Use the "-linflux" flag in order to compile your code:
```
g++ -std=c++17 -o example example.cpp -linflux
```

You can take a look the [example.cpp](example.cpp) to see how to use LibInflux.
