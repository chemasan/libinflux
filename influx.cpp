/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include "influx.hpp"
#include <string>
#include <vector>
#include <map>
#include <cstdint>
#include <variant>
#include <cpr/cpr.h>
#include <nlohmann/json.hpp>

using namespace std;
using namespace cpr;
using json = nlohmann::json;

namespace influx
{
	string escape(const string& unescaped)
	{
		string escaped = "";
		for (char c : unescaped)
		{
			switch(c)
			{
				case '\\':
					escaped.append("\\\\");
					break;
				case ' ':
					escaped.append("\\ ");
					break;
				case '\t':
					escaped.append("\\t");
					break;
				case '"':
					escaped.append("\\\"");
					break;
				case '\'':
					escaped.append("\\'");
					break;
				case '\n':
					escaped.append("\\n");
					break;
				default:
					escaped.append(1,c);
			}
		}
		return escaped;
	}


	string toString(const Data& value)
	{
		switch (value.index())
		{
			case 0:
				return to_string(get<int64_t>(value)) + "i"s;
				break;
			case 1:
				return to_string(get<double>(value));
				break;
			case 2:
				return ("\""s + get<string>(value) + "\""s);
				break;
			case 3:
				if (get<bool>(value)) return "true"s;
				else return "false"s;
				break;
			default:
				throw runtime_error("Not implemented conversion to string");
		}
	}

	string Record::toString()
	{
		string result = "";
		result += escape(measurement);
		for (auto tag : tags)
		{
			result +=",";
			result += escape(tag.first);
			result +="=";
			result += escape(tag.second);
		}
		result += " ";
		bool firstiter = true;
		for (auto field : fields)
		{
			if (firstiter) firstiter = false;
			else result += ",";
			result += escape(field.first);
			result += "=";
			result += influx::toString(field.second);
		}
		result += " ";
		result += to_string(timestamp);
		return result;
	}

	class Influx::Session : public cpr::Session {};

	Influx::Influx(const string& url, const string& user, const string& password)
	{
		this->url = url;
		session = new Session();
		session->SetOption(Header{{"Accept-Encoding", "identity"},{"Connection","keep-alive"},{"Keep-Alive","timeout=120, max=0"}});
		session->SetOption(Authentication{user,password});
	}
	Influx::Influx(Influx&& other)
	{
		swap(this->session,other.session);
		this->url = move(other.url);
	}
	Influx& Influx::operator=(Influx&& other)
	{
		swap(this->session,other.session);
		this->url = move(other.url);
		return (*this);
	}
	Influx::~Influx()
	{
		delete session;
	}
	void Influx::createDb(const string& dbname)
	{
		session->SetOption(Url{url + "/query"s});
		string query = "CREATE DATABASE " + escape(dbname);
		session->SetOption(Payload{{"q", query}});
		Response r = session->Post();
		if (r.status_code != 200)
		{
			string serverError;
			try
			{
				json response = json::parse(r.text);
				serverError = response["error"];
			} catch (const exception& e) {}
			string message = "ERROR creating database '"s + dbname + "' on server '"s + url + "'."s;
			if (!serverError.empty()) message += " Server replied with message: " + serverError;
			throw runtime_error(message);
		}
	}
	void Influx::write(const string& dbname, vector<Record>& records, unsigned long maxSize)
	{
		session->SetOption(Url{url + "/write?db="s + escape(dbname)});
		Response r;

		Body body;
		unsigned long curSize = 0;
		for (auto record : records)
		{
			string recordData = record.toString();
			unsigned long recordDataSize = recordData.size() + 1;
			if (recordDataSize > maxSize) throw runtime_error("The size of a single record is bigger than the given maximum request body");
			curSize += recordDataSize;
			if (curSize > maxSize)
			{
				session->SetOption(body);
				r = session->Post();
				if (r.status_code < 200 || r.status_code >299) throw runtime_error("ERROR writing to database '"s + dbname + "' on server '"s + url + "'"s);
				curSize = recordDataSize;
				body.clear();
			}
			body += recordData + "\n"s;
		}
		session->SetOption(body);
		r = session->Post();
		if (r.status_code < 200 || r.status_code >299)
		{
			string serverError;
			try
			{
				json response = json::parse(r.text);
				serverError = response["error"];
			} catch (const exception& e) {}
			string message = "ERROR writing to database '"s + dbname + "' on server '"s + url + "'."s;
			if (!serverError.empty()) message += " Server replied with message: " + serverError;
			throw runtime_error(message);
		}

	}
}
