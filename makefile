# Copyright (c) 2020 Jose Manuel Sanchez Madrid.
# This file is licensed under MIT license. See file LICENSE for details.

CXXFLAGS:=-std=c++17 -fPIC -g
.DEFAULT_GOAL:=libs
.PHONY: clean deps cleandeps test libs install
PREFIX ?= /usr/local
export CPATH:=.:build/include:$(PREFIX)/include
export LIBRARY_PATH:=.:build/lib:$(PREFIX)/lib
export LD_LIBRARY_PATH:=.:build/lib:$(PREFIX)/lib

libinflux.so: influx.o
	$(CXX) -shared -Wl,-soname,$@.0 -lcpr -o $@.0 $^
	ln -f -s $@.0 $@
libinflux.a: influx.o
	ar rcs $@ $^
libs: libinflux.so libinflux.a
install: libs
	install -m 644 -D --target-directory "$(PREFIX)/include" influx.hpp
	install -m 644 -D --target-directory "$(PREFIX)/lib" libinflux.so.0 libinflux.a
	cp -P -t "${PREFIX}/lib" libinflux.so
	ldconfig || true
uninstall:
	rm -f "$(PREFIX)/include/influx.hpp"
	rm -f "$(PREFIX)/lib/libinflux.a"
	rm -f "$(PREFIX)/lib/libinflux.so"
	rm -f "$(PREFIX)/lib/libinflux.so.0"
	ldconfig || true
example: example.o libinflux.so
	$(CXX) -L. -linflux -o $@ $<
unittests: unittests.o libinflux.so
	$(CXX) -L. -linflux -o $@ $<
test: unittests
	./unittests
clean:
	rm -f *.o *.a *.so *.so.* ./prueba ./example ./unittests
deps:
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/cpr-1.3.0.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/catch-2.4.0.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/json-3.9.1.yaml"
cleandeps:
	rm -rf ./build/
