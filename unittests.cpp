/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "influx.hpp"

using namespace influx;

TEST_CASE("toString returns the proper string representation of the given Data")
{
	CHECK( toString(33l) == "33i" );
	CHECK( toString(3.33) == "3.330000" );
	CHECK( toString(true) == "true"s );
	CHECK( toString(false) == "false"s);
	CHECK( toString("this is a string"s) == "\"this is a string\""s );
}
TEST_CASE("Record::toString() returns a text representation of the record suitable for import into influxdb")
{
	Record r1{ "mymeasure"s,{},{{"value"s,33.3}},1234};
	CHECK( r1.toString() == "mymeasure value=33.300000 1234"s);
	Record r2{ "mymeasure"s,{},{{"fvalue"s,33.3},{"ivalue"s,33l},{"bvalue"s,true}},1234};
	CHECK( r2.toString() == "mymeasure bvalue=true,fvalue=33.300000,ivalue=33i 1234"s);
	Record r3{ "mymeasure"s,{{"mytag1"s,"tagvalue1"s},{"mytag2"s,"tagvalue2"s}}, {{"ivalue"s,33l},{"bvalue"s,false},{"svalue"s,"my string"s}}, 1234};
	CHECK( r3.toString() == "mymeasure,mytag1=tagvalue1,mytag2=tagvalue2 bvalue=false,ivalue=33i,svalue=\"my string\" 1234"s );
	Record r4{ "my measure"s,{{"my tag"s,"tag value 1"s},{"\"my\" tag"s,"tag's value"s}}, {{"value's name"s,"value's value"s}}, 1234};
	CHECK( r4.toString() == "my\\ measure,\\\"my\\\"\\ tag=tag\\'s\\ value,my\\ tag=tag\\ value\\ 1 value\\'s\\ name=\"value's value\" 1234"s );
}

